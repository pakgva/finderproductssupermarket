class GondolaProduct < ActiveRecord::Base
  attr_accessible :gondola_id, :product_id, :x, :y, :level, :body

  belongs_to :product
  belongs_to :gondola

  validates_presence_of :gondola_id, :product_id, :x, :y, :level, :body

  paginates_per 10
end
