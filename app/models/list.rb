class List < ActiveRecord::Base
  attr_accessible :name, :person_profile_id, :state
  has_and_belongs_to_many :products
  belongs_to :person_profile

  validates_presence_of :name
end
