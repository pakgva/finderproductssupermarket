class Aisle < ActiveRecord::Base
  attr_accessible :name, :number, :state

  belongs_to :supermarket
  has_many :gondolas, dependent: :destroy

  paginates_per 10

  validates_presence_of :name

  def number_name
    "#{ number } - #{ name }"
  end
end
