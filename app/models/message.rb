class Message < ActiveRecord::Base
  attr_accessible :email, :message, :name, :subject, :user_id

  validates_presence_of :email, :message, :name, :subject

  paginates_per 10

  belongs_to :user
end
