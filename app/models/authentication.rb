class Authentication < ActiveRecord::Base
  belongs_to :user
  attr_accessible :oauth_expires_at, :oauth_token, :provider, :uid, :user_id

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |authentication|
      authentication.provider = auth.provider
      authentication.uid = auth.id
      authentication.oauth_token = auth.credentials.token
      authentication.oauth_expires_at = Time.at(auth.credentials.expires_at)
    end
  end
end
