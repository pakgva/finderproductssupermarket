class Product < ActiveRecord::Base
  attr_accessible :name, :description, :photo, :state, :sub_category_id, :brand_id, :link

  has_and_belongs_to_many :lists
  has_many :product_supermarkets
  has_many :supermarkets, through: :product_supermarkets
  has_many :gondola_products
  has_many :gondolas, through: :gondola_products
  belongs_to :sub_category
  belongs_to :brand

  has_attached_file :photo, default_url: '/assets/default_product.png'

  paginates_per 10

  validates_attachment_size :photo, less_than: 2.megabytes
  validates_attachment_content_type :photo, content_type: ['image/jpeg', 'image/png']

  before_post_process :check_file_size

  validates_presence_of :name, :description, :sub_category_id, :brand_id, :link

  def category_sub_category
    "#{ category_name } - #{ sub_category_name }"
  end

  def check_file_size
    valid?
    errors[:photo_file_size].blank?
  end

  def x_y
    gondola_products.map { |gp| { x: gp.x, y: gp.y } }
  end

  def category_name
    sub_category.category.name
  end

  def sub_category_name
    sub_category.name
  end

  def brand_name
    brand.name
  end

  def id_name_brand
    "#{ id } - #{ name } - #{ brand_name }"
  end
end
