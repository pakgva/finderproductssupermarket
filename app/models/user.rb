class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :person_profile_attributes
  # attr_accessible :title, :body

  has_many :authentications
  has_one :person_profile, dependent: :destroy
  has_many :messages, dependent: :destroy

  accepts_nested_attributes_for :person_profile

  paginates_per 10

  def full_name
    person_profile.full_name
  end

  def cart_count
    $redis.scard "cart#{ id }"
  end
end
