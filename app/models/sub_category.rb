class SubCategory < ActiveRecord::Base
  attr_accessible :category_id, :name, :state

  has_many :products
  belongs_to :category

  paginates_per 10

  validates_presence_of :name, :category_id
end
