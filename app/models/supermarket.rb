class Supermarket < ActiveRecord::Base
  attr_accessible :address, :latitude, :longitude, :name

  validates_presence_of :name, :address
  validate :geolocation_for_supermarket
  has_many :product_supermarkets
  has_many :products, through: :product_supermarkets
  has_many :gondolas
  has_many :aisles

  private

  def geolocation_for_supermarket
    if latitude.blank? || longitude.blank?
      errors.add :base, "Debe seleccionar un punto en el mapa."
    end
  end
end
