require "apriori_algorithm"
require "yaml"

class Apriori < ActiveRecord::Base
  attr_accessible :confidence, :support, :database, :execution_time

  has_attached_file :database

  validates_attachment_size :database, less_than: 5.megabytes
  validates_attachment_content_type :database, content_type: ['text/csv', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
  validates_presence_of :database, :confidence, :support

  before_post_process :check_file_size

  paginates_per 10

  def generate_rules
    start = DateTime.now
    dataset = data_set_from_csv
    item_set = AprioriAlgorithm::ItemSet.new(dataset)
    rules = item_set.mine(support, confidence)
    finish = DateTime.now
    File.open("public/rules/#{ finish.strftime('%B-%Y') }.yml", "w+") do |file|
      file.write(rules.to_yaml)
    end
    update_attribute(:execution_time, calculate_execution_time(start, finish))
  end

  private

  def calculate_execution_time(start, finish)
    t = ((finish - start) * 24 * 60 * 60).to_i
    mm, ss = t.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)
    "%d dias, %d horas, %d minutos y %d segundos" % [dd, hh, mm, ss]
  end

  def check_file_size
    valid?
    errors[:photo_file_size].blank?
  end

  def data_set_from_csv
    dataset = {}
    index = 1
    CSV.foreach(database.path) do |row|
      dataset["t#{ index }".to_sym] = row
      index += 1
    end
    dataset
  end
end
