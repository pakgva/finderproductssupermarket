class PersonProfile < ActiveRecord::Base
  attr_accessible :birthday, :dni, :last_names, :names, :phone, :sex, :user_id, :photo

  belongs_to :user
  has_many :lists

  has_attached_file :photo, default_url: 'https://secure.gravatar.com/avatar/000000000000000000000000000000000?d=mm&s=100'

  validates_presence_of :names, :last_names
  validates_length_of :dni, is: 8, allow_nil: true
  validates_attachment_size :photo, less_than: 2.megabytes
  validates_attachment_content_type :photo, content_type: ['image/jpeg', 'image/png']

  before_post_process :check_file_size

  def first_name
    names.split.first
  end

  def first_last_name
    last_names.split.first
  end

  def full_name
    first_name + " " + first_last_name
  end

  def check_file_size
    valid?
    errors[:photo_file_size].blank?
  end
end
