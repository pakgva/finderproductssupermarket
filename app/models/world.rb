class World < ActiveRecord::Base
  attr_accessible :categories_count, :name, :state, :xend, :xstart, :yend, :ystart

  has_many :categories

  validates_presence_of :name, :xend, :xstart, :yend, :ystart

  paginates_per 10
end
