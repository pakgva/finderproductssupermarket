class ProductSupermarket < ActiveRecord::Base
  attr_accessible :product_id, :supermarket_id, :unit_price

  belongs_to :product
  belongs_to :supermarket

  validates_presence_of :unit_price

  def cart_action(current_user_id)
    if $redis.sismember "cart#{ current_user_id }", id
      "Remover del"
    else
      "Agregar al"
    end
  end
end
