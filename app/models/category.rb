class Category < ActiveRecord::Base
  attr_accessible :gondolas_count, :name, :state, :world_id, :xstart, :xend, :ystart, :yend

  has_many :sub_categories, dependent: :destroy
  belongs_to :aisle
  belongs_to :world, counter_cache: true

  paginates_per 10

  validates_presence_of :name, :xend, :xstart, :yend, :ystart, :world_id
end
