class Gondola < ActiveRecord::Base
  attr_accessible :aisle_id, :code, :columns_count, :name, :rows_count, :state, :xstart, :xend, :ystart, :yend, :category_id, :color, :supermarket_id

  belongs_to :aisle
  belongs_to :category, counter_cache: true
  belongs_to :supermarket
  has_many :gondola_products
  has_many :products, through: :gondola_products

  paginates_per 10

  validates_presence_of :code, :aisle_id, :columns_count, :rows_count, :name, :xstart, :ystart, :xend, :yend, :category_id, :color, :supermarket_id

  def code_name
    "#{ code } - #{ name }"
  end

  def red
    rgb = color.match(/#(..)(..)(..)/)
    rgb[1].hex
  end

  def green
    rgb = color.match(/#(..)(..)(..)/)
    rgb[2].hex
  end

  def blue
    rgb = color.match(/#(..)(..)(..)/)
    rgb[3].hex
  end
end
