class AislesController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin
  before_filter :find_supermarket

  def index
    @aisles = @supermarket.aisles.page(params[:page])
  end

  def new
    @aisle = Aisle.new
  end

  def edit
    @aisle = Aisle.find(params[:id])
  end

  def create
    @aisle = @supermarket.aisles.build(params[:aisle])

    if @aisle.save
      flash[:notice] = 'Pasillo fue satisfactoriamente creado.'
      redirect_to supermarket_aisles_path(@supermarket)
    else
      render action: "new"
    end
  end

  def update
    @aisle = Aisle.find(params[:id])

    if @aisle.update_attributes(params[:aisle])
      flash[:notice] = 'Pasillo fue satisfactoriamente actualizado.'
      redirect_to supermarket_aisles_path(@supermarket)
    else
      render action: "edit"
    end
  end

  def destroy
    @aisle = Aisle.find(params[:id])
    @aisle.destroy

    flash[:notice] = 'Pasillo fue satisfactoriamente eliminado.'
    redirect_to supermarket_aisles_path(@supermarket)
  end

  private

  def find_supermarket
    @supermarket = Supermarket.find(params[:supermarket_id])
  end
end
