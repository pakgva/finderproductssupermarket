class SearchController < ApplicationController
  before_filter :authenticate_user!

  def index
    @supermarket = Supermarket.find(params[:supermarket_id])
    @categories = Category.all
    @brands = Brand.all
  end

  def paginate
    @product_supermarkets = response_scope
    @product_supermarkets = @product_supermarkets.page(params.has_key?(:page) ? params[:page] : 1).per(9)
    render layout: false
  end

  def selected
    product = Product.find(params[:product_id])
    render partial: "selected", locals: { product: product }
  end

  def update_sub_categories
    @sub_categories = SubCategory.where(category_id: params[:category_id]).order(:name)
    render layout: false
  end

  private

  def response_scope
    product_supermarkets = ProductSupermarket.scoped(
      conditions: ["product_supermarkets.supermarket_id = ?", params[:supermarket_id]]
    )

    if params[:product].present?
      product_supermarkets = product_supermarkets.scoped(
        include: [:product],
        conditions: [
          ActiveRecord::Base.connection.adapter_name == "PostgreSQL" ? "products.name ILIKE :val" : "products.name LIKE :val",
          { val: "%#{ params[:product] }%" }
        ]
      )
    end

    if params.has_key?(:category_id)
      product_supermarkets = product_supermarkets.scoped(
        include: [product: :sub_category],
        conditions: ["sub_categories.category_id = ?", params[:category_id]]
      )
    end

    if params.has_key?(:sub_category_ids)
      product_supermarkets = product_supermarkets.scoped(
        include: [product: :sub_category],
        conditions: ["sub_categories.id IN (?)", params[:sub_category_ids]]
      )
    end

    if params.has_key?(:brand_ids)
      product_supermarkets = product_supermarkets.scoped(
        include: [product: :brand],
        conditions: ["sizes.id IN (?)", params[:brand_ids]]
      )
    end

    if params.has_key?(:price_ids)
      product_supermarkets = product_supermarkets.scoped(
        conditions: [condition_for_price(params[:price_ids])]
      )
    end

    order_by =
      case params[:order_by]
      when "1" then "product_supermarkets.unit_price DESC"
      when "2" then "product_supermarkets.unit_price ASC"
      when "3" then "product_supermarkets.created_at DESC"
      end

    product_supermarkets = product_supermarkets.scoped(
      order: order_by
    )

    product_supermarkets
  end

  def condition_for_price(price_ids)
    conditions = []

    price_ids.each do |p_id|
      condition =
        case p_id
        when "1" then "(product_supermarkets.unit_price >= 1 AND product_supermarkets.unit_price <= 10)"
        when "2" then "(product_supermarkets.unit_price >= 10 AND product_supermarkets.unit_price <= 25)"
        when "3" then "(product_supermarkets.unit_price >= 25 AND product_supermarkets.unit_price <= 50)"
        when "4" then "(product_supermarkets.unit_price >= 50 AND product_supermarkets.unit_price <= 100)"
        when "5" then "(product_supermarkets.unit_price >= 100 AND product_supermarkets.unit_price <= 200)"
        end
      conditions << condition
    end

    conditions.join(" OR ")
  end
end
