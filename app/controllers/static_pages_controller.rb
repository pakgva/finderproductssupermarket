class StaticPagesController < ApplicationController
  def index
  end

  def about_us
  end

  def how_fpsm_works
  end

  def contact_us
    @message = Message.new

    if current_user.present?
      @message.name = current_user.full_name
      @message.email = current_user.email
    end
  end

  def submit_message
    @message = Message.new(params[:message])

    if current_user.present?
      @message.user = current_user
    end

    if @message.save
      NoReply.send_message(@message).deliver
      flash[:notice] = "Mensaje enviado, nosotros le contestaremos lo antes posible."
      redirect_to root_path
    else
      render :contact_us
    end
  end
end
