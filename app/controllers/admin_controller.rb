class AdminController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin

  def users
    @users = User.includes(:person_profile).order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/users' }
    end
  end

  def worlds
    @worlds = World.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/worlds' }
    end
  end

  def supermarkets
    @supermarkets = Supermarket.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/supermarkets' }
    end
  end

  def categories
    @categories = Category.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/categories' }
    end
  end

  def sub_categories
    @sub_categories = SubCategory.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/sub_categories' }
    end
  end

  def gondolas
    @gondolas = Gondola.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/gondolas' }
    end
  end

  def products
    @products = Product.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/products' }
    end
  end

  def brands
    @brands = Brand.order('created_at desc').page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/brands' }
    end
  end

  def messages
    @messages = Message.includes(:user => :person_profile).order("created_at desc").page(params[:page])
    respond_to do |format|
      format.html
      format.js { render partial: 'admin/messages' }
    end
  end

  def message
    @message = Message.includes(:user => :person_profile).find(params[:id])
  end
end
