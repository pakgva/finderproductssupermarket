class BrandsController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin

  def new
    @brand = Brand.new
  end

  def edit
    @brand = Brand.find(params[:id])
  end

  def create
    @brand = Brand.new(params[:brand])

    if @brand.save
      flash[:notice] = "Marca #{ @brand.name } fue satisfactoriamente creada."
      redirect_to brands_admin_index_path
    else
      render action: "new"
    end
  end

  def update
    @brand = Brand.find(params[:id])

    if @brand.update_attributes(params[:brand])
      flash[:notice] = "Marca #{ @brand.name } fue satisfactoriamente actualizada."
      redirect_to brands_admin_index_path
    else
      render action: "edit"
    end
  end

  def destroy
    @brand = Brand.find(params[:id])
    @brand.destroy

    flash[:notice] = "Marca #{ @brand.name } fue satisfactoriamente eliminada."
    redirect_to brands_admin_index_path
  end
end
