class ProductSupermarketsController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin
  before_filter :find_product

  def index
    @product_supermarkets = @product.product_supermarkets.page(params[:page])
  end

  def new
    @product_supermarket = ProductSupermarket.new
  end

  def edit
    @product_supermarket = ProductSupermarket.find(params[:id])
  end

  def create
    @product_supermarket = @product.product_supermarkets.build(params[:product_supermarket])

    if @product_supermarket.save
      flash[:notice] = "Producto #{ @product.name } fue asignado al supermercado satisfactoriamente."
      redirect_to product_product_supermarkets_path(@product)
    else
      render action: "new"
    end
  end

  def update
    @product_supermarket = ProductSupermarket.find(params[:id])

    if @product_supermarket.update_attributes(params[:product_supermarket])
      flash[:notice] = "La asignacion al supermercado del producto #{ @product.name } fue editada satisfactoriamente."
      redirect_to product_product_supermarkets_path(@product)
    else
      render action: "edit"
    end
  end

  def destroy
    @product_supermarket = ProductSupermarket.find(params[:id])
    @product_supermarket.destroy

    flash[:notice] = "La asignacion al supermercado del producto #{ @product.name } fue eliminada satisfactoriamente."
    redirect_to product_product_supermarkets_path(@product)
  end

  private

  def find_product
    @product = Product.find(params[:product_id])
  end
end
