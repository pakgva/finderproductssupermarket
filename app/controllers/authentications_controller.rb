class AuthenticationsController < ApplicationController
  def create
    authentication = Authentication.from_omniauth(env["omniauth.auth"])
    session[:user_id] = authentication.user.id
    redirect_to root_url
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end
end