class SupermarketsController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin
  skip_before_filter :only_admin, only: :index

  def index
    @supermarkets = Supermarket.all
    render layout: "application"
  end

  def show
    @supermarket = Supermarket.find(params[:id])
  end

  def new
    @supermarket = Supermarket.new
  end

  def edit
    @supermarket = Supermarket.find(params[:id])
  end

  def create
    @supermarket = Supermarket.new(params[:supermarket])

    if @supermarket.save
      flash[:notice] = "Supermercado #{ @supermarket.name } fue satisfactoriamente creado."
      redirect_to @supermarket
    else
      render action: "new"
    end
  end

  def update
    @supermarket = Supermarket.find(params[:id])

    if @supermarket.update_attributes(params[:supermarket])
      flash[:notice] = "Supermercado #{ @supermarket.name } fue satisfactoriamente actualizado."
      redirect_to @supermarket
    else
      render action: "edit"
    end
  end

  def destroy
    @supermarket = Supermarket.find(params[:id])
    @supermarket.destroy

    flash[:notice] = "Supermercado #{ @supermarket.name } fue satisfactoriamente eliminado."
    redirect_to supermarkets_admin_index_path
  end
end
