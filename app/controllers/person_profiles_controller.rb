class PersonProfilesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_person_profile

  def edit
  end

  def update
    if @profile.update_attributes(params[:person_profile])
      flash[:title] = 'Perfil actualizado'
      flash[:notice] = 'Tu informacion del perfil ha sido actualizada satisfactoriamente.'
      redirect_to edit_person_profile_path(@profile)
    else
      @profile.errors.delete(:photo)
      render 'edit'
    end
  end

  private

  def find_person_profile
    @profile = PersonProfile.find(params[:id])
  end
end
