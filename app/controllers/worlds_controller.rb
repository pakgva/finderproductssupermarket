class WorldsController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin

  def new
    @world = World.new
  end

  def edit
    @world = World.find(params[:id])
  end

  def create
    @world = World.new(params[:world])

    if @world.save
      flash[:notice] = "Mundo #{ @world.name } fue satisfactoriamente creado."
      redirect_to worlds_admin_index_path
    else
      render action: "new"
    end
  end

  def update
    @world = World.find(params[:id])

    if @world.update_attributes(params[:world])
      flash[:notice] = "Mundo #{ @world.name } fue satisfactoriamente actualizado."
      redirect_to worlds_admin_index_path
    else
      render action: "edit"
    end
  end

  def destroy
    @world = World.find(params[:id])
    @world.destroy

    flash[:notice] = "Mundo #{ @world.name } fue satisfactoriamente eliminado."
    redirect_to worlds_admin_index_path
  end
end
