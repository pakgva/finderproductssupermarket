require 'square'
require 'yaml'

class FinderProductsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_for_product_ids, only: :new

  def new
    ProductSupermarket.scoped(
      conditions: ["supermarket_id = ? AND product_id IN (?)", params[:supermarket_id], params[:product_ids]]
    ).map{ |ps| $redis.sadd current_user_cart, ps.id }

    redirect_to cart_path(current_user.id)
  end

  def index
    @supermarket = Supermarket.find(params[:supermarket_id])
    set_up_layout
    @products = Product.scoped(
      joins: [:supermarkets],
      conditions: ["product_supermarkets.id IN (?)", params[:product_supermarket_ids]]
    )
    get_positions_from_products
    get_squares_from_positions
    render json: find_path
  end

  def in_cart
    cart_ids = $redis.smembers current_user_cart
    @cart_product_supermarkets = ProductSupermarket.find(cart_ids)
    render layout: false
  end

  def selected
    product = Product.find(params[:product_id])
    render partial: "selected", locals: { product: product }
  end

  def suggested
    time = Time.now
    rules = YAML.load_file("public/rules/#{ time.strftime('%B-%Y') }.yml")
    products = ProductSupermarket.find(params[:product_supermarket_ids]).map{ |ps| ps.product_id.to_s }
    logger.info products
    products_suggested = []
    rules.each do |key, value|
      rule = key.split("=>")
      rule_products = rule[0].split(",")
      products_suggested << rule[1].split(",") if (rule_products - products).empty?
    end
    products_suggested.flatten!
    products_suggested = products_suggested.uniq - products

    @products_suggested = Product.scoped(
      conditions: ["products.id IN (?)", products_suggested]
    )
    @supermarket = Supermarket.find(params[:supermarket_id])

    render layout: false
  end

  def show_product_in_gondola
    @gondola_product = GondolaProduct.find_by_product_id_and_x_and_y(
      params[:product_id],
      params[:location][:x],
      params[:location][:y]
    )
    @product = @gondola_product.product
    @gondola = @gondola_product.gondola
    render layout: false
  end

  private

  def set_up_layout
    @layout = Grid.new(@supermarket.width, @supermarket.height)
    set_up_impassable_layout
  end

  def current_user_cart
    "cart#{ current_user.id }"
  end

  def set_up_impassable_layout
    gondolas = Gondola.scoped(conditions: ["supermarket_id = ?", params[:supermarket_id]])
    gondolas.each do |g|
      (g.xstart..g.xend).each do |x|
        (g.ystart..g.yend).each do |y|
          @layout.make_impassable(x, y)
        end
      end
    end
  end

  def get_positions_from_products
    positions = @products.map{ |p| p.x_y }

    @positions = cartprod(*positions)

    @positions.map{ |position| position.unshift({ x: 0, y: 0 }) }
  end

  def get_squares_from_positions
    @squares = @positions.map do |position|
      position.map{ |p| @layout.square_at(p[:x], p[:y]) }
    end
    @costs = @squares.map{ |s| 0 }
  end

  def cartprod(*args)
    result = [[]]
    while [] != args
      t, result = result, []
      b, *args = args

      t.each do |a|
        b.each do |n|
          result << a + [n]
        end
      end
    end
    result
  end

  def find_path
    @paths = []
    @locations = @squares.map(&:dup)
    @squares.each_with_index do |square, index|
      current_path = []
      current_point = square.shift
      until square.empty?
        calculate_fastest(current_point, index)
        next_point = square.shift
        cost, path = current_point.path_to(next_point)
        current_path << path
        @costs[index] += cost
        current_point = next_point
      end
      cost, path = current_point.path_to(@layout.square_at(0, @supermarket.height - 1))
      current_path << path
      @costs[index] += cost
      current_path.flatten!
      @paths << current_path
    end

    final_path, final_location = calculate_cheapest_path
    {
      path: final_path.map{ |s| { x: s.x, y: s.y } },
      locations: final_location.map{ |s| { x: s.x, y: s.y } },
      products: @products.map(&:id)
    }
  end

  def calculate_cheapest_path
    ordered_costs = @costs.sort
    paths = []
    locations = []

    ordered_costs.each do |cost|
      index = @costs.index(cost)
      paths << @paths.at(index)
      locations << @locations.at(index)
    end

    locations.first.shift

    return paths.first, locations.first
  end

  def calculate_fastest(current_point, index)
    @squares[index].sort_by! { |s| manhattan_distance(current_point, s) }
  end

  def manhattan_distance(current_point, square)
    (current_point.x - square.x).abs + (current_point.y - square.y).abs
  end
end
