class GondolaProductsController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin
  before_filter :find_product

  def index
    @gondola_products = @product.gondola_products.page(params[:page])
  end

  def new
    @gondola_product = GondolaProduct.new
  end

  def edit
    @gondola_product = GondolaProduct.find(params[:id])
  end

  def create
    @gondola_product = @product.gondola_products.build(params[:gondola_product])

    if @gondola_product.save
      flash[:notice] = "Producto #{ @product.name } fue asignado a la gondola satisfactoriamente."
      redirect_to product_gondola_products_path(@product)
    else
      render action: "new"
    end
  end

  def update
    @gondola_product = GondolaProduct.find(params[:id])

    if @gondola_product.update_attributes(params[:gondola_product])
      flash[:notice] = "La asignacion a la gondola del producto #{ @product.name } fue editada satisfactoriamente."
      redirect_to product_gondola_products_path(@product)
    else
      render action: "edit"
    end
  end

  def destroy
    @gondola_product = GondolaProduct.find(params[:id])
    @gondola_product.destroy

    flash[:notice] = "La asignacion a la gondola del producto #{ @product.name } fue eliminada satisfactoriamente."
    redirect_to product_gondola_products_path(@product)
  end

  private

  def find_product
    @product = Product.find(params[:product_id])
  end
end
