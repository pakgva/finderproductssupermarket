require 'square'
require 'yaml'

class AssociationRulesController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin

  def index
    @aprioris = Apriori.order('created_at desc').page(params[:page])
  end

  def new
    @apriori = Apriori.new
  end

  def edit
    @apriori = Apriori.find(params[:id])

    if @apriori.execution_time.present?
      flash[:alert] = "No se puede editar la configuracion, las reglas ya han sido generadas."
      redirect_to association_rules_path
    end
  end

  def create
    @apriori = Apriori.new(params[:apriori])

    if @apriori.save
      flash[:notice] = 'La configuracion fue satisfactoriamente creada.'
      redirect_to association_rules_path
    else
      render action: "new"
    end
  end

  def update
    @apriori = Apriori.find(params[:id])

    if @apriori.execution_time.present?
      flash[:alert] = "No se puede editar la configuracion, las reglas ya han sido generadas."
      redirect_to association_rules_path
    end

    if @apriori.update_attributes(params[:brand])
      flash[:notice] = "La configuracion fue satisfactoriamente actualiza."
      redirect_to association_rules_path
    else
      render action: "edit"
    end
  end

  def show
    @apriori = Apriori.find(params[:id])
  end

  def flare
    @apriori = Apriori.find(params[:id])
    @association_rules = YAML.load_file("public/rules/#{ @apriori.updated_at.strftime('%B-%Y') }.yml")

    products, packageNames = products_in_rules
    matrix = matrix_of_rules(products)

    render json: { packageNames: packageNames, matrix: matrix }
  end

  def paginate
    @apriori = Apriori.find(params[:id])
    @rules = response_scope
    render layout: false
  end

  def show_products_in_layout
    @apriori = Apriori.find(params[:id])
    @antecedents = params[:antecedent_ids]
    @consequents = params[:consequent_ids]
    @products = []
    @products << @antecedents
    @products << @consequents
    @products.flatten!
    @supermarket = Supermarket.first
    @rule = "#{ @antecedents.join(", ") } => #{ @consequents.join(", ") }"
    @categories = Gondola.scoped(
        conditions: ["supermarket_id = ?", @supermarket.id]
      ).map{ |g| { name: g.category.name, color: g.color } }.uniq
  end

  def paths
    @apriori = Apriori.find(params[:id])
    @supermarket = Supermarket.find(params[:supermarket_id])
    @products = Product.find(params[:product_ids])
    set_up_layout
    get_positions_from_products
    get_squares_from_positions
    render json: find_path
  end

  private

  def set_up_layout
    @layout = Grid.new(@supermarket.width, @supermarket.height)
    set_up_impassable_layout
  end

  def set_up_impassable_layout
    gondolas = Gondola.scoped(conditions: ["supermarket_id = ?", params[:supermarket_id]])
    gondolas.each do |g|
      (g.xstart..g.xend).each do |x|
        (g.ystart..g.yend).each do |y|
          @layout.make_impassable(x, y)
        end
      end
    end
  end

  def get_positions_from_products
    positions = @products.map{ |p| p.x_y }
    @positions = cartprod(*positions)
  end

  def cartprod(*args)
    result = [[]]
    while [] != args
      t, result = result, []
      b, *args = args

      t.each do |a|
        b.each do |n|
          result << a + [n]
        end
      end
    end
    result
  end

  def get_squares_from_positions
    @squares = @positions.map do |position|
      position.map{ |p| @layout.square_at(p[:x], p[:y]) }
    end
    @costs = @squares.map{ |s| 0 }
  end

  def find_path
    @paths = []
    @locations = @squares.map(&:dup)
    @squares.each_with_index do |square, index|
      current_path = []
      current_point = square.shift
      calculate_fastest(current_point, index)
      until square.empty?
        next_point = square.shift
        cost, path = current_point.path_to(next_point)
        current_path << path
        @costs[index] += cost
        current_point = next_point
      end
      current_path.flatten!
      @paths << current_path
    end

    final_path, final_location = calculate_cheapest_path
    {
      path: final_path.map{ |s| { x: s.x, y: s.y } },
      locations: final_location.map{ |s| { x: s.x, y: s.y } },
      products: @products.map(&:id)
    }
  end

  def calculate_cheapest_path
    ordered_costs = @costs.sort
    paths = []
    locations = []

    ordered_costs.each do |cost|
      index = @costs.index(cost)
      paths << @paths.at(index)
      locations << @locations.at(index)
    end

    return paths.first, locations.first
  end

  def calculate_fastest(current_point, index)
    @squares[index].sort_by! { |s| manhattan_distance(current_point, s) }
  end

  def manhattan_distance(current_point, square)
    (current_point.x - square.x).abs + (current_point.y - square.y).abs
  end

  def response_scope
    antecedent = params[:filter][:antecedent] if params[:filter].present?
    consequent = params[:filter][:consequent] if params[:filter].present?

    @rules = YAML.load_file("public/rules/#{ @apriori.updated_at.strftime('%B-%Y') }.yml")

    if antecedent.present? || consequent.present?
      @rules = @rules.select do |key, value|
        if antecedent.present? && consequent.present?
          key.match(/^#{ antecedent }=>#{ consequent }$/)
        elsif antecedent.present?
          key.match(/^#{ antecedent }=>/)
        elsif consequent.present?
          key.match(/\=\>#{ consequent }$/)
        end
      end
    end

    @rules
  end

  def products_in_rules
    products = []
    @association_rules.each_key do |key|
      association_rule = key.split("=>")
      antecedent = association_rule[0].split(",")
      consequent = association_rule[1].split(",")
      products << antecedent
      products << consequent
    end
    products = products.flatten!.uniq
    products.sort!
    packageNames = []
    products.each do |p|
      product = Product.scoped(conditions: ["id = ?", p]).first
      if product.nil?
        packageNames << p
      else
        packageNames << product.id_name_brand
      end
    end
    return products, packageNames
  end

  def matrix_of_rules(packageNames)
    matrix = []

    packageNames.each do |p|
      row = packageNames.map{ |p| 0 }
      @association_rules.each_key do |key|
        association_rule = key.split("=>")
        antecedent = association_rule[0].split(",")
        consequent = association_rule[1].split(",")

        if consequent.index(p).nil?
          next
        else
          antecedent.each do |a|
            index = packageNames.index(a)
            row[index] = 1
          end
        end
      end
      matrix << row
    end

    matrix
  end
end
