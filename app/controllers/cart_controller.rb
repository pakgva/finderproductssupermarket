class CartController < ApplicationController
  before_filter :authenticate_user!

  def show
    cart_ids = $redis.smembers current_user_cart
    if cart_ids.empty?
      flash[:notice] = "Debe tener al menos un producto agregado al carrito, comience seleccionando un supermercado."
      redirect_to supermarkets_path
      return
    else
      cart_product_supermarkets = ProductSupermarket.find(cart_ids)
      @products = cart_product_supermarkets.map(&:product_id)
      @supermarket = cart_product_supermarkets.first.supermarket
      @product_supermarkets = ProductSupermarket.scoped(
        conditions: ["supermarket_id = ?", @supermarket.id]
      )
      @categories = Gondola.scoped(
        conditions: ["supermarket_id = ?", @supermarket.id]
      ).map{ |g| { name: g.category.name, color: g.color } }.uniq
    end
  end

  def add
    $redis.sadd current_user_cart, params[:product_supermarket_id]
    render json: current_user.cart_count, status: 200
  end

  def remove
    $redis.srem current_user_cart, params[:product_supermarket_id]
    render json: current_user.cart_count, status: 200
  end

  def product_supermarkets_selected
    cart_ids = $redis.smembers current_user_cart
    render json: cart_ids
  end

  private

  def current_user_cart
    "cart#{ current_user.id }"
  end
end
