class GondolasController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin
  skip_before_filter :only_admin, only: :points_from

  def show
    @gondola = Gondola.find(params[:id])
  end

  def new
    @gondola = Gondola.new
  end

  def edit
    @gondola = Gondola.find(params[:id])
  end

  def create
    @gondola = Gondola.new(params[:gondola])

    if @gondola.save
      flash[:notice] = 'Gondola fue satisfactoriamente creada.'
      redirect_to @gondola
    else
      render action: "new"
    end
  end

  def update
    @gondola = Gondola.find(params[:id])

    if @gondola.update_attributes(params[:gondola])
      flash[:notice] = 'Gondola fue satisfactoriamente actualizada.'
      redirect_to @gondola
    else
      render action: "edit"
    end
  end

  def destroy
    @gondola = Gondola.find(params[:id])
    @gondola.destroy

    flash[:notice] = 'Gondola fue satisfactoriamente eliminada.'
    redirect_to gondolas_admin_index_path
  end

  def points_from
    gondolas = Gondola.scoped(conditions: ["supermarket_id = ?", params[:supermarket_id]])
    points = gondolas.map{ |g| { xstart: g.xstart, xend: g.xend, ystart: g.ystart, yend: g.yend, red: g.red, green: g.green, blue: g.blue } }
    render json: points
  end
end
