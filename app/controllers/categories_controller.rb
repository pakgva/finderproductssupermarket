class CategoriesController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin

  def new
    @category = Category.new
  end

  def edit
    @category = Category.find(params[:id])
  end

  def create
    @category = Category.new(params[:category])

    if @category.save
      flash[:notice] = 'Categoria fue satisfactoriamente creada.'
      redirect_to categories_admin_index_path
    else
      render action: "new"
    end
  end

  def update
    @category = Category.find(params[:id])

    if @category.update_attributes(params[:category])
      flash[:notice] = 'Categoria fue satisfactoriamente actualizada.'
      redirect_to categories_admin_index_path
    else
      render action: "edit"
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    flash[:notice] = 'Categoria fue satisfactoriamente eliminada.'
    redirect_to categories_admin_index_path
  end

  def update_sub_categories
    sub_categories = SubCategory.select([:id, :name]).where(category_id: params[:id]).order(:name)
    render json: { sub_categories: sub_categories }
  end
end
