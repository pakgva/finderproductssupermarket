class ListsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_for_product_supermarket_ids, only: :new
  before_filter :get_lists, only: [:index, :new, :create]

  def new
    @products = Product.scoped(
      joins: [:supermarkets],
      conditions: ["product_supermarkets.id IN (?)", params[:product_supermarket_ids]]
    )
    @list = List.new
  end

  def create
    @products = Product.find_all_by_id(params[:product_ids])
    @list = List.new(params[:list])
    @list.person_profile = current_user.person_profile
    @products.each do |p|
      @list.products << p
    end

    if @list.save
      $redis.del current_user_cart
      flash[:notice] = "La lista #{ @list.name } fue satisfactoriamente creada."
      redirect_to lists_path
    else
      render 'new'
    end
  end

  def add_products
    @list = List.find(params[:list][:id])
    @products = Product.find(params[:product_ids])
    @products.each do |p|
      @list.products << p unless @list.products.include?(p)
    end

    if @list.save
      $redis.del current_user_cart
      flash[:notice] = "La lista #{ @list.name } ha sido actualizada."
    else
      flash[:error] = "Hubo un error y la lista #{ @list.name } no ha sido actualizada."
    end

    redirect_to lists_path
  end

  def destroy_product
    @list = List.find(params[:id])
    @product = Product.find(params[:product_id])
    @list.products.delete(@product)
    flash[:notice] = "#{ @product.name } ha sido removida de la lista #{ @list.name }."

    if @list.products.empty?
      @list.destroy
      flash[:notice] = "La lista #{ @list.name } ha sido eliminada."
    end

    redirect_to lists_path
  end

  def destroy
    @list = List.find(params[:id])
    @list.destroy

    flash[:notice] = "La lista #{ @list.name } fue satisfactoriamente eliminada."
    redirect_to lists_path
  end

  private

  def get_lists
    @lists = List.find_all_by_person_profile_id(current_user.person_profile)
  end

  def current_user_cart
    "cart#{ current_user.id }"
  end
end
