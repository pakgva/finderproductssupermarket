class SubCategoriesController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin
  before_filter :find_category

  def index
    @sub_categories = @category.sub_categories.page(params[:page])
  end

  def new
    @sub_category = SubCategory.new
  end

  def edit
    @sub_category = SubCategory.find(params[:id])
  end

  def create
    @sub_category = @category.sub_categories.build(params[:sub_category])

    if @sub_category.save
      flash[:notice] = "Sub Categoria #{ @sub_category.name } para #{ @category.name } fue satisfactoriamente creada."
      redirect_to category_sub_categories_path(@category)
    else
      render action: "new"
    end
  end

  def update
    @sub_category = SubCategory.find(params[:id])

    if @sub_category.update_attributes(params[:sub_category])
      flash[:notice] = "Sub Categoria #{ @sub_category.name } para #{ @category.name } fue satisfactoriamente actualizada."
      redirect_to category_sub_categories_path(@category)
    else
      render action: "edit"
    end
  end

  def destroy
    @sub_category = SubCategory.find(params[:id])
    @sub_category.destroy

    flash[:notice] = "Sub Categoria #{ @sub_category.name } para #{ @category.name } fue satisfactoriamente eliminada."
    redirect_to category_sub_categories_path(@category)
  end

  private

  def find_category
    @category = Category.find(params[:category_id])
  end
end
