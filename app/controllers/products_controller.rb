class ProductsController < ApplicationController
  layout 'admin'

  before_filter :authenticate_user!
  before_filter :only_admin

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def edit
    @product = Product.find(params[:id])
  end

  def destroy_assignment_gondola_product
    @product = Product.find(params[:id])
    @gondola_product = GondolaProduct.find(params[:gondola_product])

    @gondola_product.destroy

    flash[:notice] = 'La asignacion a la gondola fue eliminada satisfactoriamente.'
    redirect_to show_assignmments_product_path(@product)
  end

  def create
    @product = Product.new(params[:product])

    if @product.save
      flash[:notice] = 'Producto fue satisfactoriamente creado.'
      redirect_to @product
     else
      @product.errors.delete(:photo)
      render action: "new"
    end
  end

  def update
    @product = Product.find(params[:id])

    if @product.update_attributes(params[:product])
      flash[:notice] = 'Producto fue satisfactoriamente actualizado.'
      redirect_to @product
    else
      @product.errors.delete(:photo)
      render action: "edit"
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    flash[:notice] = 'Producto fue satisfactoriamente eliminado.'
    redirect_to products_admin_index_path
  end
end
