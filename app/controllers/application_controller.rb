class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
    if resource.admin?
      admin_index_path
    else
      root_path
    end
  end

  def only_admin
    unless current_user.admin?
      flash[:title] = 'Permiso denegado'
      flash[:alert] = 'Usted no tiene permiso para acceder a la pagina.'

      redirect_to root_path
    end
  end

  def check_for_product_supermarket_ids
    unless params.has_key?(:product_supermarket_ids)
      flash[:error] = 'Por favor selecciona al menos un producto para agregar a la lista.'
      redirect_to :back
    end
  end

  def check_for_product_ids
    unless params.has_key?(:product_ids)
      flash[:error] = 'La lista debe tener al menos un producto para generar la ruta.'
      redirect_to :back
    end
  end
end
