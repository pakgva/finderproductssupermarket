class NoReply < ActionMailer::Base
  default from: "finderproductssm@gmail.com"

  def send_message(message)
    @message = message
    mail(to: "finderproductssm@gmail.com", subject: "No Responder - FPSM Contactenos")
  end
end
