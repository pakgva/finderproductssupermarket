module ApplicationHelper
  def error_messages!(resource)
    return "" if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
                      count: resource.errors.count,
                      resource: resource.class.model_name.human.downcase)
    html = <<-HTML
      <div class="error_messages alert alert-dismissable alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4><strong>#{ sentence }</strong></h4>
        <ul>#{ messages }</ul>
      </div>
    HTML

    html.html_safe
  end

  def report_errors(obj)
    @errors = obj.errors if obj.errors.any?
  end

  def stylesheet(*args)
    content_for(:stylesheet) { stylesheet_link_tag(*args) }
  end

  def javascript(*args)
    content_for(:javascript) { javascript_include_tag(*args) }
  end
end
