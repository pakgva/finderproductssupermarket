module PersonProfilesHelper
  def build_person_profile
    resource.build_person_profile if resource.person_profile.nil?
  end
end
