class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :email
      t.string :message
      t.string :name
      t.string :subject
      t.integer :user_id

      t.timestamps
    end
  end
end
