class CreateGondolaProducts < ActiveRecord::Migration
  def change
    create_table :gondola_products do |t|
      t.integer :gondola_id
      t.integer :product_id
      t.integer :x
      t.integer :y

      t.timestamps
    end
  end
end
