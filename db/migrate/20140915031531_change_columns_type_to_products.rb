class ChangeColumnsTypeToProducts < ActiveRecord::Migration
  def up
    change_column :products, :x, :integer
    change_column :products, :y, :integer
  end

  def down
    change_column :products, :x, :decimal, :precision => 6, :scale => 2
    change_column :products, :y, :decimal, :precision => 6, :scale => 2
  end
end
