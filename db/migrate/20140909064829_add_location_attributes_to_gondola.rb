class AddLocationAttributesToGondola < ActiveRecord::Migration
  def change
    add_column :gondolas, :xstart, :decimal, :precision => 6, :scale => 2
    add_column :gondolas, :xend, :decimal, :precision => 6, :scale => 2
    add_column :gondolas, :ystart, :decimal, :precision => 6, :scale => 2
    add_column :gondolas, :yend, :decimal, :precision => 6, :scale => 2
    add_column :gondolas, :category_id, :integer
    add_column :gondolas, :bodies_count, :integer
    add_column :gondolas, :levels_count, :integer
  end
end
