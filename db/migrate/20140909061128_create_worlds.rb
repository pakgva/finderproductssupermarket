class CreateWorlds < ActiveRecord::Migration
  def change
    create_table :worlds do |t|
      t.string :name
      t.decimal :xstart, :precision => 6, :scale => 2
      t.decimal :ystart, :precision => 6, :scale => 2
      t.decimal :xend, :precision => 6, :scale => 2
      t.decimal :yend, :precision => 6, :scale => 2
      t.integer :categories_count
      t.boolean :state, :default => true

      t.timestamps
    end
  end
end
