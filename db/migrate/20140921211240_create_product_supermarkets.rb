class CreateProductSupermarkets < ActiveRecord::Migration
  def change
    create_table :product_supermarkets do |t|
      t.integer :supermarket_id
      t.integer :product_id
      t.decimal :unit_price, precision: 8, scale: 2

      t.timestamps
    end
  end
end
