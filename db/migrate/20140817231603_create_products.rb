class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.boolean :state, :default => true
      t.integer :sub_category_id
      t.integer :row
      t.integer :column
      t.integer :gondola_id

      t.timestamps
    end
  end
end
