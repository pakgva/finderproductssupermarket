class AddBodyAndLevelToGondolaProducts < ActiveRecord::Migration
  def change
    add_column :gondola_products, :body, :integer, default: 1
    add_column :gondola_products, :level, :integer, default: 1
  end
end
