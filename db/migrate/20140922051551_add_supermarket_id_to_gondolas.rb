class AddSupermarketIdToGondolas < ActiveRecord::Migration
  def change
    add_column :gondolas, :supermarket_id, :integer
  end
end
