class CreateAprioris < ActiveRecord::Migration
  def up
    create_table :aprioris do |t|
      t.decimal :confidence, precision: 5, scale: 2, default: 50
      t.decimal :support, precision: 5, scale: 2, default: 50
      t.attachment :database
      t.timestamps
    end

    Apriori.create(confidence: 50, support: 50)
  end

  def down
    drop_table :aprioris
  end
end
