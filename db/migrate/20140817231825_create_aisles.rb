class CreateAisles < ActiveRecord::Migration
  def change
    create_table :aisles do |t|
      t.string :name
      t.boolean :state, :default => true

      t.timestamps
    end
  end
end
