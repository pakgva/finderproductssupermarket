class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :name
      t.integer :gondola_id
      t.decimal :zstart, :precision => 6, :scale => 2
      t.decimal :zend, :precision => 6, :scale => 2
      t.integer :products_count
      t.boolean :state, :default => true

      t.timestamps
    end
  end
end
