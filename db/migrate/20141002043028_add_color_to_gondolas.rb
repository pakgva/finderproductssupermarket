class AddColorToGondolas < ActiveRecord::Migration
  def change
    add_column :gondolas, :color, :string, default: "#ff0000"
  end
end
