class AddSupermarketIdToAisles < ActiveRecord::Migration
  def up
    add_column :aisles, :supermarket_id, :integer

    supermarket_id = Supermarket.first.id

    Aisle.all.each do |a|
      a.update_attribute(:supermarket_id, supermarket_id);
    end
  end

  def down
    remove_column :aisles, :supermarket_id
  end
end
