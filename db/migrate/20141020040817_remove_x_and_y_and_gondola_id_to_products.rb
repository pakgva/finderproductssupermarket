class RemoveXAndYAndGondolaIdToProducts < ActiveRecord::Migration
  def up
    remove_column :products, :x
    remove_column :products, :y
    remove_column :products, :gondola_id
    remove_column :products, :row
    remove_column :products, :column
  end

  def down
    puts "No se puede agregar estas columnas a la tabla producto."
  end
end
