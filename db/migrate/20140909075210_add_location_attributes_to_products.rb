class AddLocationAttributesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :xstart, :decimal, :precision => 6, :scale => 2
    add_column :products, :xend, :decimal, :precision => 6, :scale => 2
    add_column :products, :body_id, :integer
    add_column :products, :level_id, :integer
  end
end
