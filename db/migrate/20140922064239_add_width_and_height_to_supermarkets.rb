class AddWidthAndHeightToSupermarkets < ActiveRecord::Migration
  def change
    add_column :supermarkets, :width, :integer
    add_column :supermarkets, :height, :integer
  end
end
