class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name

      t.timestamps
    end
    add_column :products, :brand_id, :integer
  end
end
