class CreateProductsLists < ActiveRecord::Migration
  def self.up
    create_table :lists_products, :id => false do |t|
      t.belongs_to :list
      t.belongs_to :product
    end

    add_index :lists_products, [:list_id, :product_id]
  end

  def self.down
    drop_table :lists_products
  end
end
