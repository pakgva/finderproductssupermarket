class CreateBodies < ActiveRecord::Migration
  def change
    create_table :bodies do |t|
      t.string :name
      t.integer :gondola_id
      t.decimal :xstart, :precision => 6, :scale => 2
      t.decimal :xend, :precision => 6, :scale => 2
      t.integer :products_count
      t.boolean :state, :default => true

      t.timestamps
    end
  end
end
