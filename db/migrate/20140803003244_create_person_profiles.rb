class CreatePersonProfiles < ActiveRecord::Migration
  def change
    create_table :person_profiles do |t|
      t.string :names
      t.string :last_names
      t.string :dni
      t.string :phone
      t.datetime :birthday
      t.boolean :sex
      t.integer :user_id

      t.timestamps
    end
  end
end
