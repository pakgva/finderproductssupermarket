class DropLevels < ActiveRecord::Migration
  def up
    drop_table :levels
    remove_column :products, :level_id
    remove_column :gondolas, :levels_count
  end

  def down
    puts "No se puede restablecer la tabla nivel."
  end
end
