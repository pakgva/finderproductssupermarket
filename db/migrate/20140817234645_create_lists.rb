class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :name
      t.boolean :state, :default => true
      t.integer :person_profile_id

      t.timestamps
    end
  end
end
