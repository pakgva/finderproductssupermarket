class CreateSupermarkets < ActiveRecord::Migration
  def change
    create_table :supermarkets do |t|
      t.string :name
      t.string :address
      t.decimal :latitude, precision: 12, scale: 7
      t.decimal :longitude, precision: 12, scale: 7

      t.timestamps
    end
  end
end
