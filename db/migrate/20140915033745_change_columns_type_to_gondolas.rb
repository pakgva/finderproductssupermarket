class ChangeColumnsTypeToGondolas < ActiveRecord::Migration
  def up
    change_column :gondolas, :xstart, :integer
    change_column :gondolas, :ystart, :integer
    change_column :gondolas, :xend, :integer
    change_column :gondolas, :yend, :integer
  end

  def down
    change_column :gondolas, :xstart, :decimal, :precision => 6, :scale => 2
    change_column :gondolas, :ystart, :decimal, :precision => 6, :scale => 2
    change_column :gondolas, :xend, :decimal, :precision => 6, :scale => 2
    change_column :gondolas, :yend, :decimal, :precision => 6, :scale => 2
  end
end
