class AddExecutionTimeToAprioris < ActiveRecord::Migration
  def change
    add_column :aprioris, :execution_time, :string
  end
end
