class FixColumnsNameToProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.rename :xstart, :x
      t.rename :xend, :y
    end
  end
end
