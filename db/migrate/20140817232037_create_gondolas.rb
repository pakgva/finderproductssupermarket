class CreateGondolas < ActiveRecord::Migration
  def change
    create_table :gondolas do |t|
      t.string :name
      t.boolean :state, :default => true
      t.integer :rows_count
      t.integer :columns_count
      t.integer :aisle_id

      t.timestamps
    end
  end
end
