class AddLocationAttributesToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :xstart, :decimal, :precision => 6, :scale => 2
    add_column :categories, :xend, :decimal, :precision => 6, :scale => 2
    add_column :categories, :ystart, :decimal, :precision => 6, :scale => 2
    add_column :categories, :yend, :decimal, :precision => 6, :scale => 2
    add_column :categories, :world_id, :integer
    add_column :categories, :gondolas_count, :integer
  end
end
