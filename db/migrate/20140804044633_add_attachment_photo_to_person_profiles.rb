class AddAttachmentPhotoToPersonProfiles < ActiveRecord::Migration
  def self.up
    add_attachment :person_profiles, :photo
  end

  def self.down
    remove_attachment :person_profiles, :photo
  end
end
