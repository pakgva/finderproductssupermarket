class DropBodies < ActiveRecord::Migration
  def up
    drop_table :bodies
    remove_column :products, :body_id
    remove_column :gondolas, :bodies_count
  end

  def down
    puts "No se puede restablecer la tabla cuerpo."
  end
end
