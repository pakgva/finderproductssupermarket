# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141118043220) do

  create_table "aisles", :force => true do |t|
    t.string   "name"
    t.boolean  "state",          :default => true
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "number"
    t.integer  "supermarket_id"
  end

  create_table "aprioris", :force => true do |t|
    t.decimal  "confidence",            :precision => 5, :scale => 2, :default => 50.0
    t.decimal  "support",               :precision => 5, :scale => 2, :default => 50.0
    t.string   "database_file_name"
    t.string   "database_content_type"
    t.integer  "database_file_size"
    t.datetime "database_updated_at"
    t.datetime "created_at",                                                            :null => false
    t.datetime "updated_at",                                                            :null => false
    t.string   "execution_time"
  end

  create_table "brands", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.boolean  "state",                                        :default => true
    t.datetime "created_at",                                                     :null => false
    t.datetime "updated_at",                                                     :null => false
    t.decimal  "xstart",         :precision => 6, :scale => 2
    t.decimal  "xend",           :precision => 6, :scale => 2
    t.decimal  "ystart",         :precision => 6, :scale => 2
    t.decimal  "yend",           :precision => 6, :scale => 2
    t.integer  "world_id"
    t.integer  "gondolas_count"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "gondola_products", :force => true do |t|
    t.integer  "gondola_id"
    t.integer  "product_id"
    t.integer  "x"
    t.integer  "y"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "body",       :default => 1
    t.integer  "level",      :default => 1
  end

  create_table "gondolas", :force => true do |t|
    t.string   "name"
    t.boolean  "state",          :default => true
    t.integer  "rows_count"
    t.integer  "columns_count"
    t.integer  "aisle_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "code"
    t.integer  "xstart"
    t.integer  "xend"
    t.integer  "ystart"
    t.integer  "yend"
    t.integer  "category_id"
    t.integer  "supermarket_id"
    t.string   "color",          :default => "#ff0000"
  end

  create_table "lists", :force => true do |t|
    t.string   "name"
    t.boolean  "state",             :default => true
    t.integer  "person_profile_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "lists_products", :id => false, :force => true do |t|
    t.integer "list_id"
    t.integer "product_id"
  end

  add_index "lists_products", ["list_id", "product_id"], :name => "index_lists_products_on_list_id_and_product_id"

  create_table "messages", :force => true do |t|
    t.string   "email"
    t.string   "message"
    t.string   "name"
    t.string   "subject"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "person_profiles", :force => true do |t|
    t.string   "names"
    t.string   "last_names"
    t.string   "dni"
    t.string   "phone"
    t.datetime "birthday"
    t.boolean  "sex"
    t.integer  "user_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "product_supermarkets", :force => true do |t|
    t.integer  "supermarket_id"
    t.integer  "product_id"
    t.decimal  "unit_price",     :precision => 8, :scale => 2
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.boolean  "state",              :default => true
    t.integer  "sub_category_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "brand_id"
    t.string   "link"
    t.string   "description"
  end

  create_table "sub_categories", :force => true do |t|
    t.string   "name"
    t.boolean  "state",       :default => true
    t.integer  "category_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "supermarkets", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.decimal  "latitude",   :precision => 12, :scale => 7
    t.decimal  "longitude",  :precision => 12, :scale => 7
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "width"
    t.integer  "height"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,     :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "worlds", :force => true do |t|
    t.string   "name"
    t.decimal  "xstart",           :precision => 6, :scale => 2
    t.decimal  "ystart",           :precision => 6, :scale => 2
    t.decimal  "xend",             :precision => 6, :scale => 2
    t.decimal  "yend",             :precision => 6, :scale => 2
    t.integer  "categories_count"
    t.boolean  "state",                                          :default => true
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
  end

end
