# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# populate Supermarkets

Supermarket.create("address"=>"Panamericana Sur", "height"=>104, "latitude"=>"-12.1475587", "longitude"=>"-76.9816196", "name"=>"Atocongo", "width"=>142)
Supermarket.create("address"=>"Av. Angamos Este", "height"=>142, "latitude"=>"-12.112236", "longitude"=>"-77.0112419", "name"=>"Open Plaza Angamos", "width"=>104)

# populate Gondolas

Gondola.create("aisle_id":1, "category_id":1, "code":"ABB", "color":"#38ef68", "columns_count":8, "name":"Fruta 1", "rows_count":5, "state":true, "supermarket_id":2, "xend":134, "xstart":96, "yend":101, "ystart":99)
Gondola.create("aisle_id":1, "category_id":1, "code":"FRUTA2", "color":"#38ef68", "columns_count":6, "name":"Fruta 2", "rows_count":5, "state":true, "supermarket_id":2, "xend":128, "xstart":124, "yend":95, "ystart":81)
Gondola.create("aisle_id":1, "category_id":1, "code":"FRUTA3", "color":"#38ef68", "columns_count":4, "name":"Fruta 3", "rows_count":5, "state":true, "supermarket_id":2, "xend":120, "xstart":117, "yend":95, "ystart":81)
Gondola.create("aisle_id":1, "category_id":1, "code":"FRUTA4", "color":"#38ef68", "columns_count":4, "name":"Fruta 4", "rows_count":5, "state":true, "supermarket_id":2, "xend":113, "xstart":110, "yend":95, "ystart":81)
Gondola.create("aisle_id":1, "category_id":1, "code":"FRUTA5", "color":"#38ef68", "columns_count":4, "name":"Fruta 5", "rows_count":5, "state":true, "supermarket_id":2, "xend":106, "xstart":102, "yend":95, "ystart":81)
Gondola.create("aisle_id":1, "category_id":1, "code":"LACTEO1", "color":"#00c6ff", "columns_count":4, "name":"Lacteo 1", "rows_count":5, "state":true, "supermarket_id":2, "xend":94, "xstart":65, "yend":101, "ystart":99)
Gondola.create("aisle_id":1, "category_id":1, "code":"PANADERIA1", "color":"#ff8b00", "columns_count":4, "name":"Panaderia 1", "rows_count":5, "state":true, "supermarket_id":2, "xend":139, "xstart":132, "yend":4, "ystart":2)
Gondola.create("aisle_id":1, "category_id":1, "code":"PANADERIA2", "color":"#ff8b00", "columns_count":4, "name":"Panaderia 2", "rows_count":5, "state":true, "supermarket_id":2, "xend":139, "xstart":137, "yend":26, "ystart":8)
Gondola.create("aisle_id":1, "category_id":1, "code":"PANADERIA3", "color":"#ff8b00", "columns_count":4, "name":"Panaderia 3", "rows_count":5, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":15, "ystart":8)
Gondola.create("aisle_id":1, "category_id":1, "code":"PANADERIA4", "color":"#ff8b00", "columns_count":5, "name":"Panaderia 4", "rows_count":5, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":26, "ystart":18)
Gondola.create("aisle_id":1, "category_id":1, "code":"COMIDA1", "color":"#ff0000", "columns_count":4, "name":"Comida Preparada 1", "rows_count":5, "state":true, "supermarket_id":2, "xend":139, "xstart":137, "yend":46, "ystart":30)
Gondola.create("aisle_id":1, "category_id":1, "code":"COMIDA2", "color":"#ff0000", "columns_count":4, "name":"Comida Preparada 2", "rows_count":5, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":36, "ystart":30)
Gondola.create("aisle_id":1, "category_id":1, "code":"COMIDA3", "color":"#ff0000", "columns_count":4, "name":"Comida Preparada 3", "rows_count":4, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":46, "ystart":40)
Gondola.create("aisle_id":1, "category_id":1, "code":"PESCADERIA1", "color":"#00fffc", "columns_count":4, "name":"Pescaderia 1", "rows_count":4, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":60, "ystart":50)
Gondola.create("aisle_id":1, "category_id":1, "code":"PESCADERIA2", "color":"#00fffc", "columns_count":3, "name":"Pescaderia 2", "rows_count":4, "state":true, "supermarket_id":2, "xend":139, "xstart":137, "yend":60, "ystart":50)
Gondola.create("aisle_id":1, "category_id":1, "code":"Gondola1", "color":"#521717", "columns_count":4, "name":"Carne 1", "rows_count":5, "state":true, "supermarket_id":2, "xend":139, "xstart":137, "yend":95, "ystart":64)
Gondola.create("aisle_id":1, "category_id":1, "code":"GONDOLA2", "color":"#521717", "columns_count":3, "name":"Carne 2", "rows_count":4, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":70, "ystart":64)
Gondola.create("aisle_id":1, "category_id":1, "code":"GONDOLA3", "color":"#521717", "columns_count":3, "name":"Carnes 3", "rows_count":4, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":76, "ystart":74)
Gondola.create("aisle_id":1, "category_id":1, "code":"GONDOLA4", "color":"#521717", "columns_count":2, "name":"Carnes 4", "rows_count":3, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":86, "ystart":80)
Gondola.create("aisle_id":1, "category_id":1, "code":"GONDOLA5", "color":"#521717", "columns_count":4, "name":"Carnes 5", "rows_count":4, "state":true, "supermarket_id":2, "xend":134, "xstart":132, "yend":95, "ystart":90)
Gondola.create("aisle_id":1, "category_id":1, "code":"CONGELADO1", "color":"#0b00ff", "columns_count":4, "name":"Congelados 1", "rows_count":3, "state":true, "supermarket_id":2, "xend":128, "xstart":122, "yend":77, "ystart":76)
Gondola.create("aisle_id":1, "category_id":1, "code":"CONGELADO2", "color":"#0b00ff", "columns_count":3, "name":"Congelados 2", "rows_count":2, "state":true, "supermarket_id":2, "xend":118, "xstart":112, "yend":77, "ystart":76)
Gondola.create("aisle_id":1, "category_id":1, "code":"CONGELADO3", "color":"#0b00ff", "columns_count":3, "name":"Congelados 3", "rows_count":4, "state":true, "supermarket_id":2, "xend":108, "xstart":102, "yend":77, "ystart":76)
Gondola.create("aisle_id":1, "category_id":1, "code":"CONGELADO4", "color":"#0b00ff", "columns_count":2, "name":"Congelados 4", "rows_count":3, "state":true, "supermarket_id":2, "xend":128, "xstart":102, "yend":72, "ystart":71)
Gondola.create("aisle_id":1, "category_id":1, "code":"BEBIDA1", "color":"#c300ff", "columns_count":4, "name":"Bebida 1", "rows_count":3, "state":true, "supermarket_id":2, "xend":128, "xstart":102, "yend":70, "ystart":69)