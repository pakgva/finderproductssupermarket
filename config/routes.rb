FinderProductsSupermarket::Application.routes.draw do
  resources :cart, only: :show do
    put 'add_to/:product_supermarket_id', to: 'cart#add', as: :add_to
    put 'remove_from/:product_supermarket_id', to: 'cart#remove', as: :remove_from
    collection do
      get 'product_supermarkets_selected'
    end
  end
  resources :supermarkets do
    resources :aisles, except: :show
  end
  resources :brands, except: [:show, :index]
  resources :worlds, except: [:show, :index]
  resources :finder_products, only: :index do
    match "/new" => "finder_products#new", via: :post, on: :collection
    collection do
      get "selected"
      get "suggested"
      get "show_product_in_gondola"
      get "in_cart"
    end
  end
  resources :association_rules do
    member do
      get 'flare'
      get 'paginate'
      get 'show_products_in_layout'
      get 'paths'
    end
  end
  resources :lists, only: [:index, :create] do
    match "/new" => "lists#new", via: :post, on: :collection
    match "/destroy" => "lists#destroy", via: :delete, on: :member
    match "/add_products" => "lists#add_products", via: :put, on: :collection
    match "/destroy_product" => "lists#destroy_product", via: :delete, on: :collection
  end
  resources :gondolas, except: :index do
    collection do
      get 'points_from'
    end
  end
  resources :products, except: :index do
    resources :gondola_products, except: :show
    resources :product_supermarkets, except: :show
  end
  resources :categories, except: [:show, :index] do
    resources :sub_categories, except: :show
    member do
      get 'update_sub_categories'
    end
  end
  resources :admin, only: :index do
    collection do
      get 'users'
      get 'categories'
      get 'aisles'
      get 'gondolas'
      get 'products'
      get 'worlds'
      get 'brands'
      get 'supermarkets'
      get 'messages'
      get 'message'
    end
  end
  resources :person_profiles, only: [:update, :edit]
  resources :search, only: :index do
    collection do
      get 'paginate'
      get 'selected'
      get 'update_sub_categories'
    end
  end
  devise_for :users
  match "/about_us" => "static_pages#about_us", as: :about_us
  match "/contact_us" => "static_pages#contact_us", as: :contact_us
  match "/how_fpsm_works" => "static_pages#how_fpsm_works", as: :how_fpsm_works
  match "/submit_message" => "static_pages#submit_message", as: :submit_message, via: :post
  root to: 'static_pages#index'

  match 'auth/:provider/callback', to: 'authentications#create'
  match 'auth/failure', to: redirect('/')
  match 'signout', to: 'authentications#destroy', as: 'signout'
end
