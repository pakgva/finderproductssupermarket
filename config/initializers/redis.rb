if Rails.env.production?
  uri = URI.parse("redis://redistogo:87b25a4648598d5ab74f9223d37cd68d@greeneye.redistogo.com:11481/")
  $redis = Redis.new(
    :host => uri.host,
    :port => uri.port,
    :password => uri.password,
    :driver => :hiredis
  )
elsif Rails.env.development?
  $redis = Redis.new(
    :driver => :hiredis
  )
end