var default_page = 1;

$(document).ready(function() {
  $(".sub_categories").hide();
  $('#sub_category').hide();
  getProducts(default_page);
  bindEvents();
  getProductsSuggested();
});

function getProducts(page){
  var params = {
    supermarket_id: $("#supermarket_id").val(),
    product: $("#product").val(),
    category_id: $("input[name='category_id']:checked").val(),
    order_by: $("#order_by a.active").data("order-id"),
    sub_category_ids: getSubCategoriesChecked(),
    brand_ids: getBrandsChecked(),
    price_ids: getPricesChecked(),
    page: page
  };

  $.get("/search/paginate", params, function(data){
    $("#products").html(data);
    $("a[data-target]").click(function(e){
      var $this, new_target, url, title;

      e.preventDefault();
      $this = $(this);

      if ($this.data('target') == 'Agregar al'){
        url = $this.data('addurl');
        new_target = "Remover del";
        title = "El producto fue agregado al carrito.";
      }
      else {
        url = $this.data('removeurl');
        new_target = "Agregar al";
        title = "El producto fue removido del carrito.";
      }

      $.ajax({
        url: url,
        type: 'put',
        success: function(data){
          $('.cart-count').html(data);
          $this.find('span').html(new_target);
          $this.data('target', new_target);
          $this.tooltip({
            title: title
          });
          $this.tooltip('show');
          setTimeout(function(){
            $this.tooltip('destroy');
          }, 2000);
          getProductsSuggested();
        }
      });
    });
  });
}

function getSubCategoriesChecked(){
  var sub_category_ids = [];

  $.each($("input[name='sub_category_ids[]']:checked"), function(){
    sub_category_ids.push($(this).val());
  });

  return sub_category_ids;
}

function getBrandsChecked(){
  var brand_ids = [];

  $.each($("input[name='brand_ids[]']:checked"), function(){
    brand_ids.push($(this).val());
  });

  return brand_ids;
}

function getPricesChecked(){
  var price_ids = [];

  $.each($("input[name='price_ids[]']:checked"), function(){
    price_ids.push($(this).val());
  });

  return price_ids;
}

function bindEvents(){
  $("input[name='category_id']").bind('change',function(){
    var params = {
      category_id: $(this).val()
    };
    $.get("/search/update_sub_categories", params, function(data){
      if (data !== ""){
        $("#sub_categories").html(data);
        $(".sub_categories").show();
      }
      else {
        $(".sub_categories").hide();
        $("#sub_categories .scroll-y").html("");
      }
    });
  });
  $("#order_by a").bind("click", function(e){
    $("#order_by a").removeClass("active");
    $(this).addClass("active");
    getProducts(default_page);
    e.preventDefault();
  });
  $("#products").delegate(".pagination a", "click", function() {
    var page = getURLParameter($(this).attr("href"), "page");
    getProducts(page);
    return false;
  });
  $("#search").bind('click', function(){
    getProducts(default_page);
  });
}

function getURLParameter(URL, sParam) {
  var i, sPageURL, sParameterName, sURLVariables;
  sPageURL = URL.split("?")[1];
  sURLVariables = sPageURL.split("&");
  i = 0;
  while (i < sURLVariables.length) {
    sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] === sParam) {
      return sParameterName[1];
    }
    i++;
  }
}

function getProductSupermarketsSelected(){
  var product_supermarket_ids = [];

  $.ajax({
    url: "/cart/product_supermarkets_selected",
    type: 'get',
    async: false,
    success: function(data){
      product_supermarket_ids = data;
      $("input[name='product_supermarket_ids[]']").remove();
      $.each(product_supermarket_ids, function(index, value){
        $("<input>").attr({
          type: 'hidden',
          name: 'product_supermarket_ids[]',
          value: value
        }).prependTo('#add-to-list');
      });
    }
  });

  return product_supermarket_ids;
}

function getProductsSuggested(){
  var product_supermarket_ids = getProductSupermarketsSelected();

  var params = {
    supermarket_id: $("#supermarket_id").val(),
    product_supermarket_ids: product_supermarket_ids
  };

  $.get("/finder_products/suggested", params, function(data){
    if (data !== ""){
      $("#products_suggested").html(data);
      $("#products_suggested .suggested_product").on('click', function(e){
        e.preventDefault();
        var $this, url;
        $this = $(this);
        url = $this.data('url');

        var params = {
          product_supermarket_id: $this.data('product-supermarket-id')
        };

        $.ajax({
          url: url,
          data: params,
          type: 'put',
          success: function(data){
            $('.cart-count').html(data);
            getProductsSuggested();
            getProducts(default_page);
          }
        });
      });
    }
    else{
      $("#products_suggested").empty();
    }
  });
}
