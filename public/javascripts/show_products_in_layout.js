var Canvas = Isomer.Canvas;
var Color = Isomer.Color;
var Shape = Isomer.Shape;
var Point = Isomer.Point;
var Path = Isomer.Path;
var angleZ = 0;
var transparency = 0.4;
var blue = new Color(0, 0, 255, transparency);
var red = new Color(255, 0 , 0);
var route;
var gondolas;
var products;
var locations;
var iso;
var canvas;
var center;

$(document).ready(function(){
  calculateHeightAndWidthCanvas();
  iso = new Isomer(document.getElementById("layout"));
  canvas = new Canvas(document.getElementById("layout"));
  center = new Point(52, 71, 0);
  bindEvents();
  getPaths();
  getGondolas();
});

function calculateHeightAndWidthCanvas(){
  var container = document.getElementById("output");
  var canvas = document.getElementById("layout");

  var width = container.offsetWidth - 30;
  var height = width * 5 / 8;

  canvas.style.width = width + "px";
  canvas.style.height = height + "px";

  var multiplier;

  if (width < 300){
    multiplier = 3;
  }
  if (width < 400){
    multiplier = 2.5;
  }
  else if (width < 500){
    multiplier = 2;
  }
  else if (width < 600){
    multiplier = 1.6;
  }
  else if (width < 700){
    multiplier = 1.4;
  }
  else{
    multiplier = 1.2;
  }

  canvas.width = width * multiplier;
  canvas.height = height * multiplier;
}

function bindEvents(){
  $(window).resize(function(){
    calculateHeightAndWidthCanvas();
    draw();
  });
}

function getPaths () {
  var product_ids = $("#layout").data('products');

  var params = {
    supermarket_id: $("#supermarket_id").val(),
    product_ids: product_ids
  };

  $.ajax({
    url: $("#layout").data('url'),
    data: params,
    dataType: 'json',
    type: 'GET',
    success: function(response){
      route = response.path;
      locations = response.locations;
      products = response.products;
      drawRoutes();
      drawLocations();
    }
  });
}

function getGondolas() {
  var params = {
    supermarket_id: $("#supermarket_id").val()
  };

  $.ajax({
    url: "/gondolas/points_from",
    data: params,
    dataType: 'json',
    type: 'GET',
    success: function(response){
      gondolas = response;
      drawGondolas();
    }
  });
}

function draw(){
  canvas.clear();
  drawRoutes();
  drawLocations();
  drawGondolas();
}

function drawGondolas(){
  for (var current = 0; current in gondolas; current++){
    var gondola = gondolas[current];
    var shape = Shape.extrude(new Path([
      Point(gondola.xstart, gondola.ystart, 0),
      Point(gondola.xstart, gondola.yend, 0),
      Point(gondola.xend, gondola.yend, 0),
      Point(gondola.xend, gondola.ystart, 0)
    ]), 2);
    var color = new Color(gondola.red, gondola.green, gondola.blue, transparency);
    iso.add(shape.rotateZ(center, angleZ), color);
  }
}

function drawRoutes() {
  for (var current = 0; current in route; current++) {
    var point = route[current];
    var shape = Shape.Prism(new Point(point.x, point.y, 0), 1, 1, 0);
    iso.add(shape.rotateZ(center, angleZ), blue);
  }
}

function drawLocations(){
  for (var current = 0; current in locations; current++) {
    var point = locations[current];
    var shape = Shape.Cylinder(new Point(point.x, point.y, 0), 1, 10, 3);
    iso.add(shape.rotateZ(center, angleZ), red);
  }
}