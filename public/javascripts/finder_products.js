var Canvas = Isomer.Canvas;
var Color = Isomer.Color;
var Shape = Isomer.Shape;
var Point = Isomer.Point;
var Path = Isomer.Path;
var angleZ = 0;
var transparency = 0.4;
var blue = new Color(0, 0, 255, transparency);
var red = new Color(255, 0 , 0);
var route;
var gondolas;
var products;
var locations;
var iso;
var canvas;
var center;

$(document).ready(function(){
  calculateHeightAndWidthCanvas();
  iso = new Isomer(document.getElementById("layout"));
  canvas = new Canvas(document.getElementById("layout"));
  center = new Point(52, 71, 0);
  getProductSupermarketsCart();
  getProductsSuggested();
  bindEvents();
  getPath();
  getGondolas();
});

function getProductSupermarketsCart(){
  $.get("/finder_products/in_cart", function(data){
    $("#products-selected").html(data);
  });
}

function calculateHeightAndWidthCanvas(){
  var container = document.getElementById("output");
  var canvas = document.getElementById("layout");

  var width = container.offsetWidth - 30;
  var height = width * 5 / 8;

  canvas.style.width = width + "px";
  canvas.style.height = height + "px";

  var multiplier;

  if (width < 300){
    multiplier = 3;
  }
  if (width < 400){
    multiplier = 2.5;
  }
  else if (width < 500){
    multiplier = 2;
  }
  else if (width < 600){
    multiplier = 1.6;
  }
  else if (width < 700){
    multiplier = 1.4;
  }
  else{
    multiplier = 1.2;
  }

  canvas.width = width * multiplier;
  canvas.height = height * multiplier;
}

function getProductSupermarketsSelected(){
  var product_supermarket_ids = [];

  $.ajax({
    url: "/cart/product_supermarkets_selected",
    type: 'get',
    async: false,
    success: function(data){
      product_supermarket_ids = data;
      $("input[name='product_supermarket_ids[]']").remove();
      $.each(product_supermarket_ids, function(index, value){
        $("<input>").attr({
          type: 'hidden',
          name: 'product_supermarket_ids[]',
          value: value
        }).prependTo('#add-to-list');
      });
    }
  });

  return product_supermarket_ids;
}

function getProductsSuggested(){
  var product_supermarket_ids = getProductSupermarketsSelected();

  var params = {
    supermarket_id: $("#supermarket_id").val(),
    product_supermarket_ids: product_supermarket_ids
  };

  $.get("/finder_products/suggested", params, function(data){
    if (data !== ""){
      $("#products_suggested").html(data);
      $("#products_suggested .suggested_product").on('click', function(e){
        e.preventDefault();
        var $this, url;
        $this = $(this);
        url = $this.data('url');

        var params = {
          product_supermarket_id: $this.data('product-supermarket-id')
        };

        $.ajax({
          url: url,
          data: params,
          type: 'put',
          success: function(){
            getProductsSuggested();
          }
        });
      });
    }
    else{
      $("#products_suggested").empty();
    }
  });
}

function getPath(){
  var product_supermarket_ids = getProductSupermarketsSelected();

  var params = {
    supermarket_id: $("#supermarket_id").val(),
    product_supermarket_ids: product_supermarket_ids
  };

  $.ajax({
    url: "/finder_products",
    data: params,
    dataType: 'json',
    type: 'GET',
    success: function(response){
      route = response.path;
      locations = response.locations;
      products = response.products;
      drawRoute();
      drawLocations();
    }
  });
}

function getGondolas(){
  var params = {
    supermarket_id: $("#supermarket_id").val()
  };

  $.ajax({
    url: "/gondolas/points_from",
    data: params,
    dataType: 'json',
    type: 'GET',
    success: function(response){
      gondolas = response;
      drawGondolas();
    }
  });
}

function bindEvents(){
  $(window).resize(function(){
    calculateHeightAndWidthCanvas();
    draw();
  });
  $("#move-right").on('click', function(){
    angleZ += Math.PI / 12;
    draw();
  });
  $("#move-left").on('click', function(){
    angleZ -= Math.PI / 12;
    draw();
  });
  $("#zoom-plus").on('click', function(){
    if (iso.scale < 20)
    {
      iso.scale += 1;
      draw();
    }
  });
  $("#zoom-minus").on('click', function(){
    iso.scale -= 1;
    draw();
  });
  $("#products-selected").delegate(".remove-product", "click", function() {

    if (confirm("¿Estás seguro?")){
      var $this, url;
      $this = $(this);
      url = $this.data('url');

      var params = {
        product_supermarket_id: $this.data('product-supermarket-id')
      };

      $.ajax({
        url: url,
        data: params,
        type: 'put',
        success: function(data){
          $(".cart-count").html(data);
          getProductSupermarketsCart();
          getProductsSuggested();
          canvas.clear();
          getPath();
          getGondolas();
        }
      });
    }

    return false;
  });
  $("#products-selected").delegate(".product-in-gondola", "click", function(){
    var product_id = parseInt($(this).data('product-id'));
    var location = getLocationFromProductId(product_id);

    var params = {
      product_id: product_id,
      location: location
    };

    $.get("/finder_products/show_product_in_gondola", params, function(data){
      $(data).modal('show');
    });
    return false;
  });
  $("#products_suggested").delegate(".suggested_product", "click", function(){
    var $this, url;
    $this = $(this);
    url = $this.data('url');

    var params = {
      product_supermarket_id: $this.data('product-supermarket-id')
    };

    $.ajax({
      url: url,
      data: params,
      type: 'put',
      success: function(data){
        $(".cart-count").html(data);
        getProductSupermarketsCart();
        getProductsSuggested();
        canvas.clear();
        getPath();
        getGondolas();
      }
    });

    return false;
  });
  $("#product_supermarkets").select2({
    allowClear: true
  }).on("change", function(){
    var product_supermarket_id_selected = $("#product_supermarkets").select2("val");
    var product_supermarket_ids = getProductSupermarketsSelected();

    if ((product_supermarket_id_selected !== "") && ($.inArray(product_supermarket_id_selected, product_supermarket_ids) == -1))
    {
      var cart_id = $("#shop-cart").data("cart-id");
      var url = "/cart/" + cart_id + "/add_to/" + product_supermarket_id_selected;

      var params = {
        product_supermarket_id: product_supermarket_id_selected
      };

      $.ajax({
        url: url,
        data: params,
        type: 'put',
        success: function(data){
          $(".cart-count").html(data);
          getProductSupermarketsCart();
          getProductsSuggested();
          canvas.clear();
          getPath();
          getGondolas();
        }
      });

      $("#product_supermarkets").select2("val", "");
    }
  });
}

function getLocationFromProductId(product_id){
  var index = products.indexOf(product_id);
  return locations[index];
}

function draw(){
  canvas.clear();
  drawRoute();
  drawLocations();
  drawGondolas();
}

function drawGondolas(){
  for (var current = 0; current in gondolas; current++){
    var gondola = gondolas[current];
    var shape = Shape.extrude(new Path([
      Point(gondola.xstart, gondola.ystart, 0),
      Point(gondola.xstart, gondola.yend, 0),
      Point(gondola.xend, gondola.yend, 0),
      Point(gondola.xend, gondola.ystart, 0)
    ]), 2);
    var color = new Color(gondola.red, gondola.green, gondola.blue, transparency);
    iso.add(shape.rotateZ(center, angleZ), color);
  }
}

function drawRoute(){
  for (var current = 0; current in route; current++) {
    var point = route[current];
    var shape = Shape.Prism(new Point(point.x, point.y, 0), 1, 1, 0);
    iso.add(shape.rotateZ(center, angleZ), blue);
  }
}

function drawLocations(){
  for (var current = 0; current in locations; current++) {
    var point = locations[current];
    var shape = Shape.Cylinder(new Point(point.x, point.y, 0), 1, 10, 3);
    iso.add(shape.rotateZ(center, angleZ), red);
  }
}