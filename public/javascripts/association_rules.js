$(document).ready(function(){
  bindEvents();
  showRulesInGraphic();
  loadRules();
});

function showRulesInGraphic(){
  var url = $("#chart_placeholder").data("flare");
  $.ajax({
    url: url,
    dataType: 'json',
    type: 'GET',
    success: function(response){
      var chart = d3.chart.dependencyWheel();

      d3.select("#chart_placeholder")
      .datum(response)
      .call(chart);

      calculateHeightAndWidthChart();
    }
  });
}

function loadRules(){
  var url = $("#chart_placeholder").data("load-rules");
  var filter, params;
  filter = {
    antecedent: $("#filter_antecedent").val(),
    consequent: $("#filter_consequent").val()
  };
  params = {
    filter: filter
  };
  $.get(url, params, function(data) {
    $("#rules").html(data);
    $("#rules table").dataTable({
      "searching": false,
      "lengthChange": false
    });
  });
}

function getURLParameter(URL, sParam) {
  var i, sPageURL, sParameterName, sURLVariables;
  sPageURL = URL.split("?")[1];
  sURLVariables = sPageURL.split("&");
  i = 0;
  while (i < sURLVariables.length) {
    sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] === sParam) {
      return sParameterName[1];
    }
    i++;
  }
}

function bindEvents(){
  $("#filter_form :submit").bind("click", function() {
    var url = $(this).closest("form").attr("action");
    loadRules(url);
    return false;
  });
  $(window).on('resize', function(){
    calculateHeightAndWidthChart();
  }).trigger("resize");
  $("#rules").delegate(".in-layout","click", function(){
    var url = $(this).attr("href");
    var antecedent_ids = $(this).data("antecedent-ids");
    var consequent_ids = $(this).data("consequent-ids");
    window.location.href = url + "?" + $.param({ antecedent_ids: antecedent_ids }) + "&" + $.param({ consequent_ids: consequent_ids });
    return false;
  });
}

function calculateHeightAndWidthChart(){
  var svg = $("#chart_placeholder svg");
  var targetWidth = svg.parent().width();
  svg.attr("width", targetWidth);
  svg.attr("height", targetWidth);

  $("#rules table").dataTable().fnDestroy();

  loadRules();
}