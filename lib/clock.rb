require File.expand_path('../../config/boot', __FILE__)
require File.expand_path('../../config/environment', __FILE__)
require 'clockwork'

include Clockwork

every(1.month, "Encolando trabajo para generar reglas") {
  Delayed::Job.enqueue Apriori.last.generate_rules
}