namespace :export do
  desc "Prints Gondolas.all in a seeds.rb way"
  task :seeds_format => :environment do
    Supermarket.order(:id).all.each do |supermarket|
      puts "Supermarket.create(#{ supermarket.serializable_hash.delete_if { |key, value| ['created_at', 'updated_at', 'id'].include?(key) }.to_s.gsub(/[{}]/,'') })"
    end
    Gondola.order(:id).all.each do |gondola|
      puts "Gondola.create(#{ gondola.serializable_hash.delete_if { |key, value| ['created_at', 'updated_at', 'id'].include?(key) }.to_s.gsub(/[{}]/,'') })"
    end
  end
end